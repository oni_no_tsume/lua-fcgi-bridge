#!/bin/sh
BIN="./lua-fcgi"
SOCKET="/tmp/lua-fcgi-test.socket"
SCRIPT="test.lua"

rm -f $SOCKET
spawn-fcgi -s $SOCKET -- $BIN $SCRIPT
chmod go+rw $SOCKET
