function send(method, uri, content, callback)
{
	var r = new XMLHttpRequest();

	r.open(method, uri, true);
	r.onreadystatechange = function()
		{
			if((r.readyState == 4) && (r.status == 200))
			{
				if(callback)
					callback(r.responseText);
			}
		};
	r.send(content);
}
