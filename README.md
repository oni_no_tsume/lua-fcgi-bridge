Description
===========

The _Lua-FCGI-Bridge_ is a small extention to `FCGI` allowing HTTP handlers to be written in _Lua_.

Structure
=========

The bridge manages HTTP requests by calling an appropriate Lua function if available. The user can write Lua functions such as `on_GET`, `on_PUT` and so on. Each of those function's return value is treated as HTTP body to be sent back to the requesting client.

To manage background operations, `IDLE_HANDLER` can be provided by the user. This optional Lua handler will be called between handling HTTP requests.

Any errors can be processed by an optional Lua function `ERROR_HANDLER`.

Additional functions (WIP)
--------------------------

There are several additonal functions that will or already have been added to _Lua-FCGI-Bridge_. You find them in `functions.c`. They are accessible via Lua table `fcgi` or - most likely - on of its sub tables `net`, `io` or `timer`.

**NOTE:** When adding further functions, do not implement them in `lua_fcgi.c`, directly! There may be issues arising with `stdio.h` and `fcgi_stdio.h`. I stumbled upon unintended behaviour of `fileno`.

Building
========

The project contains a small `Makefile` that can be used to build _Lua-FCGI-Bridge_ under Linux. The build process uses `pkg-config` to adapt to the build system and also supports different versions of Lua.

To link against your default installation of Lua, run:

```
make
```

To link against a specific Lua version (e.g. Lua5.3), run:

```
USE_LUA_LIB=lua5.3 make
```

**Note:** There is no install target yet.

**Note:** To build _Lua-FCGI-Bridge_, development packages of at least one Lua version and FCGI need to be installed.

**Tested with:** _lua5.1_, _lua5.2_, _lua5.3_, _lua5.4_, _luajit_
