/*
Copyright (c) 2014, Mathias Langer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <fcgi_config.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcgi_stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>
#include <math.h>

/* additional set of LUA functions */
#include "functions.h"

#define ALLOC(p, n)  {p = malloc(n); while(p == NULL) {sleep(1); p = malloc(n);}}

#define MAX_HANDLER_NAME_LEN  4096

static char *lua_load_result = "okay";

void error(lua_State *L, char *error_msg);

static inline int lua_getglobal_and_type(lua_State *L, const char *name)
{
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 503
	/* `lua_getglobal` already returns the type in Lua 5.3 */
	return lua_getglobal(L, name);
#else
	lua_getglobal(L, name);
	return lua_type(L, -1);
#endif
}

void default_handler(char *method, lua_State *L)
{
	printf("Content-type: text/html\r\n\r\n");

	printf("<html><body>");
	printf("<h1>Lua</h1><p>");
	printf("compile result: %s<br />", lua_load_result);
	printf("on_GET: %d<br />", lua_getglobal_and_type(L, "on_GET") == LUA_TFUNCTION);
	printf("on_PUT: %d<br />", lua_getglobal_and_type(L, "on_PUT") == LUA_TFUNCTION);
	printf("on_POST: %d<br />", lua_getglobal_and_type(L, "on_POST") == LUA_TFUNCTION);
	printf("IDLE_HANDLER: %d<br />", lua_getglobal_and_type(L, "IDLE_HANDLER") == LUA_TFUNCTION);
	printf("ERROR_HANDLER: %d<br />", lua_getglobal_and_type(L, "ERROR_HANDLER") == LUA_TFUNCTION);
	printf("</p>");
	printf("</body></html>");
	/* remove the requested globals */
	lua_pop(L, 5);
}

void reply(char *text)
{
	printf(text);
}

int handle_fcgi(lua_State *L)
{
	static char handler_name[MAX_HANDLER_NAME_LEN + 1];
	char *http_method;
	char *content;
	char *result;
	int len;

	if(FCGI_Accept() >= 0)
	{
		/* select LUA handler wrt HTTP method */
		http_method = getenv("REQUEST_METHOD");
		handler_name[0] = 0;
		strncat(handler_name, "on_", MAX_HANDLER_NAME_LEN);
		if(http_method != NULL)
			strncat(handler_name, http_method, MAX_HANDLER_NAME_LEN);

		/* sanity check */
		if(lua_getglobal_and_type(L, handler_name) == LUA_TFUNCTION)
		{
			/* push request uri */
			content = getenv("REQUEST_URI");
			if(content != NULL)
				lua_pushstring(L, content);
			else
				lua_pushstring(L, "");
			/* push http content */
			len = 0;
			content = getenv("CONTENT_LENGTH");
			if(content != NULL)
				len = strtol(content, NULL, 10);
			content = NULL;
			if(len > 0)
				ALLOC(content, len + 1);
			if(content != NULL)
			{
				fread(content, 1, len, stdin);
				content[len] = 0;
				lua_pushstring(L, content);
				free(content);
			}
			else
				lua_pushstring(L, "");

			/* invoke handler: result = handler(uri, content) */
			if(lua_pcall(L, 2, 1, 0) == 0)
			{
				/* send reply */
				result = (char*)lua_tostring(L, -1);
				reply(result);
				/* cleanup (remove result from ToS) */
				lua_pop(L, 1);
			}
			else
				error(L, (char*)lua_tostring(L, -1));
		}
		else
		{
			/* remove requested global from stack */
			lua_pop(L, 1);
			default_handler(http_method, L);
		}
		FCGI_Finish();
		return(0);
	}

	return(-1);
}

void idle(lua_State *L)
{
	char *idle_handler_name = "IDLE_HANDLER";

	/* sanity check */
	if(lua_getglobal_and_type(L, idle_handler_name) == LUA_TFUNCTION)
	{
		/* no args, no return value */
		if(lua_pcall(L, 0, 0, 0) != 0)
			error(L, (char*)lua_tostring(L, -1));
	}
	else
	{
		/* remove requested global from stack */
		lua_pop(L, 1);
	}
}

/* error message is supposed to be on top of stack */
void error(lua_State *L, char *error_msg)
{
	char *error_handler_name = "ERROR_HANDLER";

	/* sanity check */
	if(lua_getglobal_and_type(L, error_handler_name) == LUA_TFUNCTION)
	{
		/* error_message on ToS, no return value */
		if(error_msg != NULL)
			lua_pushstring(L, error_msg);
		else
			lua_pushstring(L, "NULL");
		if(lua_pcall(L, 1, 0, 0) != 0)
		{
			/* errors caused by handler are reported on STDERR */
			error_msg = (char*)lua_tostring(L, -1);
			fprintf(stderr, "DOUBLE ERROR: %s", error_msg);
			lua_pop(L, 1);
		}
		lua_pop(L, 1);
	}
	else
	{
		/* remove requested global from stack */
		lua_pop(L, 1);
	}
}

int main(int argc, char** argv)
{
	struct pollfd fds;
	lua_State *L = NULL;
	char *tmp;

	if(argc < 2)
		return(-1);

	/* init the LUA machine */
	L = luaL_newstate();
	luaL_openlibs(L);
	lua_fcgi_init(L);

	/* load handlers */
	luaL_dofile(L, argv[1]);
	tmp = (char *)lua_tostring(L, -1);
	if(tmp != NULL)
		lua_load_result = strdup(tmp);

	/* prepare poll for STDIN */
	fds.fd = 0;
	fds.events = POLLIN;

	while(1)
	{
		fds.revents = 0;
		if(poll(&fds, 1, 10) == 1)
		{
			/* action on FCGI socket -> handle FCGI connection */
			if(handle_fcgi(L) != 0)
				break;
		}

		/* call idle handler if available */
		idle(L);
	}

	/* clean-up */
	lua_close(L);

	return(0);
}
