local lookup = {}
lookup["counter"] = 1
lookup["busy"] = 0

function html_enclose(s)
	text = 'Content-type: text/html\r\n\r\n'
	text = text .. '<html>\n<body>\n' .. s .. '\n</body>\n</html>\n'
	return(text)
end

function on_GET(uri, content)
	local _, _, key = string.find(uri, "^/fcgi/(.*)")
	local value = lookup[key]
	local text = "<p><b>LUA</b> says: <i>&quot;Hello world!&quot;</i><br />"
	text = text .. "fcgi.timer.clock: " .. fcgi.timer.clock() .. "</p>"
	if value ~= nil then
		text = text .. "<p>" .. uri .. " = &quot;" .. value .. "&quot;</p>"
	end
	if key == "ENVIRONMENT" then
		local env = fcgi.getenv_list()
		if type(env) == "table" then
			-- output environment variables
			text = text .. "ENVIRONMENT<ul>"
			for k, v in pairs(env) do
				text = text .. "\n\t<li>" .. k .. " = " .. v .. "</li>"
			end
			text = text .. "\n</ul>"
		end
	end
	if key == "LIBS" then
		text = text .. "fcgi<ul>"
		for k, v in pairs(fcgi) do
			if type(v) == "table" then
				text = text .. "\n\t<li>" .. k .. "<ul>"
				for k2, v2 in pairs(v) do
					if type(v2) == "function" then
						text = text .. "\n\t\n\t<li>" .. k2 .. "</li>"
					end
				end
				text = text .. "\n\t</ul>"
			end
		end
		text = text .. "\n</ul>"
	end
	lookup["counter"] = lookup["counter"] + 1
	return(html_enclose(text))
end

function on_POST(uri, content)
	local _, _, key = string.find(uri, "^/fcgi/(.*)")
	if key ~= nil then
		lookup[key] = content
	end
	return(html_enclose("<p>" .. uri .. " set to:<pre>" .. content .. "</pre></p>"))
end

function ERROR_HANDLER(message)
	return(html_enclose("<p>ERROR:<pre>" .. message .. "</pre></p>"))
end

local ext_process_f = nil
local ext_process_fd = -1
function IDLE_HANDLER()
	if lookup["busy"] == 0 then
		-- spawn external process
		lookup["busy"] = 1
		local command = 'sleep 10; echo "done"'
		ext_process_f = io.popen(command)
	else
		-- use fcgi.io.poll to wait for changes
		ext_process_fd = fcgi.io.get_fd(ext_process_f)
		if fcgi.io.poll({ext_process_fd}, 0.01) == true then
			lookup["busy"] = 0
			lookup["result"] = ext_process_f:read("l")
			if lookup["result"] ~= nil then
				lookup["worked"] = "no"
				if lookup["result"] == "done" then
					lookup["worked"] = "yes"
				end
			end
			ext_process_f:close()
			ext_process_f = nil
			ext_process_fd = -1
		end
	end
end
