/*
Copyright (c) 2014, Mathias Langer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>

void error(lua_State *L, char *error_msg);

/* returns the complete system environment as table,
   inspired by:
   https://stackoverflow.com/questions/7633397/print-list-of-all-environment-variables
*/
static int getenv_list(lua_State *L)
{
	extern char **environ;
	char **env_var;
	char *var_name;
	char *value;

	/* sanity check */
	if(*environ == NULL)
	{
		lua_pushnil(L);
		goto end;
	}

	lua_newtable(L);
	/* traverse environment */
	for(env_var = environ; *env_var != NULL; env_var++)
	{
		/* export variable and value */
		var_name = *env_var;
		value = strchr(var_name, '=');
		if(value != NULL)
		{
			/* var_name is used as key, use only chars till separator ('=') */
			lua_pushlstring(L, var_name, value - var_name);
			/* strip separator from value */
			lua_pushstring(L, value + 1);
		}
		else
		{
			/* no seperator found => push name and empty string */
			lua_pushstring(L, var_name);
			lua_pushstring(L, "");
		}

		/* add pair(key, value) to table */
		lua_settable(L, -3);
	}

end:
	return(1);
}

/* the fcgi function table */
static const luaL_Reg fcgi_function_table[] =
{
	{"getenv_list", getenv_list},
	{NULL, NULL}
};

static int dummy(lua_State *L)
{
	return(0);
}

/* LuaJIT and Lua5.1 didn't export luaL_Stream. However, implementation
   is just the same. (taken from Lua5.2) */
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM < 502
typedef struct luaL_Stream
{
	FILE *f;
	lua_CFunction closef;
} luaL_Stream;
#endif

/*
	IN:   f -> luaL_Stream
	OUT:  fd -> INTEGER
*/
static int io_get_fd(lua_State *L)
{
	luaL_Stream *f = luaL_checkudata(L, 1, LUA_FILEHANDLE);

	if(f != NULL)
		lua_pushnumber(L, fileno(f->f));
	else
		lua_pushnil(L);

	return(1);
}

/*
	IN:  fd -> integer
	IN:  count -> integer
	OUT: buf -> string
*/
static int io_read(lua_State *L)
{
	int fd;
	size_t count;
	ssize_t n;
	char *buf;

	fd = lua_tointeger(L, 1);
	count = lua_tointeger(L, 2);
	buf = malloc(count + 1);
	if(buf != NULL)
	{
		n = read(fd, buf, count);
		if(n >= 0)
			lua_pushlstring(L, buf, n);
		else
			lua_pushnil(L);
		free(buf);
	}
	else
	{
		lua_pushnil(L);
	}

	return(1);
}

/*
	IN:  fd -> integer
	IN:  buf -> string
*/
static int io_write(lua_State *L)
{
	int fd;
	size_t count;
	char *buf;

	fd = lua_tointeger(L, 1);
	buf = (char *)lua_tolstring(L, 2, &count);
	write(fd, buf, count);

	return(0);
}

/*
	IN:  filename -> string
	OUT: fd -> integer
*/
static int io_open(lua_State *L)
{
	const char *filename;
	int fd;

	filename = lua_tostring(L, 1);
	fd = open(filename, O_RDWR);
	lua_pushinteger(L, fd);

	return(1);
}

/*
	IN:  fd -> integer
*/
static int io_close(lua_State *L)
{
	int fd;

	fd = lua_tointeger(L, 1);
	close(fd);

	return(0);
}

/* max number of descriptors to poll in one call */
#ifndef MAX_FDS_LEN
#define MAX_FDS_LEN  64
#endif

#define FDS_SET_ELEM(_fds, _i, _fd, _e)  {(_fds)[_i].fd = (_fd); (_fds)[_i].events = (_e); (_fds)[_i].revents = 0;}
/*
	IN:  fd -> table of integers
	IN:  timeout -> NUMBER (in seconds)
	OUT: success -> boolean
	OUT: ready_list -> table of boolean
*/
static int io_poll(lua_State *L)
{
	lua_Number timeout;
	struct pollfd fds[MAX_FDS_LEN];
	int fd;
	int ret;
	int len = 0;
	int i;

	if(lua_istable(L, 1))
	{
		/* traverse a table of file descriptors */
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 502
		lua_len(L, 1);
		len = lua_tointeger(L, -1);
		lua_pop(L, 1);
#else
		len = lua_objlen(L, 1);
#endif
		if(len > MAX_FDS_LEN)
			len = MAX_FDS_LEN;
		for(i = 0; i < len; i++)
		{
			/* note: LUA tables start at 1 */
			lua_rawgeti(L, 1, i + 1);
			/* current element is on top of the stack ... */
			fd = lua_tointeger(L, -1);
			/* ... and needs to be removed */
			lua_pop(L, 1);
			FDS_SET_ELEM(fds, i, fd, POLLIN);
		}
	}
	timeout = lua_tonumber(L, 2);

	ret = 1;
	if(poll(fds, len, floor(timeout * 1000)) <= 0)
		ret = 0;

	/* success marker */
	lua_pushboolean(L, ret);
	/* ready state for each requested file descriptor */
	lua_createtable(L, len, 0);
	for(i = 0; i < len; i++)
	{
		ret = ((fds[i].revents & POLLIN) != 0);
		lua_pushboolean(L, ret);
		lua_rawseti(L, -2, i + 1);
	}

	return(2);
}

/* additional IO functions */
static const luaL_Reg fcgi_io_function_table[] =
{
	{"get_fd", io_get_fd},
	{"set_mode", dummy},
	{"read", io_read},
	{"write", io_write},
	{"open", io_open},
	{"close", io_close},
	{"poll", io_poll},
	{NULL, NULL}
};

/*
	IN:  sec -> NUMBER
*/
static int timer_sleep(lua_State *L)
{
	lua_Number sec;
	struct timespec t;

	sec = lua_tonumber(L, 1);
	t.tv_sec = floor(sec);
	t.tv_nsec = floor((sec - t.tv_sec) * 1000000000);
	nanosleep(&t, NULL);

	return(0);
}

/*
	OUT:  timestamp -> NUMBER (in seconds [float])
*/
static int timer_clock(lua_State *L)
{
	struct timespec t;
	lua_Number sec;

	clock_gettime(CLOCK_MONOTONIC, &t);
	sec = t.tv_sec + (lua_Number)(t.tv_nsec) / 1000000000.0;
	lua_pushnumber(L, sec);

	return(1);
}

/* additional timing functions */
static const luaL_Reg fcgi_timer_function_table[] =
{
	{"clock", timer_clock},
	{"sleep", timer_sleep},
	{NULL, NULL}
};

/* additional network functions */
static const luaL_Reg fcgi_net_function_table[] =
{
	{"accept", dummy},
	{"recv", dummy},
	{"send", dummy},
	{"tcp_server", dummy},
	{"uds_server", dummy},
	{"tcp_client", dummy},
	{"uds_client", dummy},
	{"udp_socket", dummy},
	{NULL, NULL}
};

#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM < 502
void helper_setfuncs(lua_State *L, const luaL_Reg *l)
{
	if(l == NULL)
		return;

	while((l->name != NULL) && (l->func != NULL))
	{
		/* linking 'l->func' to table on stack with key 'l->name' */
		lua_pushcfunction(L, l->func);
		lua_setfield(L, -2, l->name);
		/* next entry */
		l++;
	}
}
#endif

int lua_fcgi_init(lua_State* L)
{
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 502
	lua_newtable(L);
	lua_setglobal(L, "fcgi");
	lua_getglobal(L, "fcgi");
	luaL_setfuncs(L, fcgi_function_table, 0);
#else
	luaL_register(L, "fcgi", fcgi_function_table);
#endif

	/* create sub-lib "fcgi.io" */
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 502
	luaL_getsubtable(L, -1, "io");
	luaL_setfuncs(L, fcgi_io_function_table, 0);
	lua_pop(L, 1);
#else
	lua_newtable(L);
	helper_setfuncs(L, fcgi_io_function_table);
	lua_setfield(L, -2, "io");
#endif

	/* create sub-lib "fcgi.net" */
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 502
	luaL_getsubtable(L, -1, "net");
	luaL_setfuncs(L, fcgi_net_function_table, 0);
	lua_pop(L, 1);
#else
	lua_newtable(L);
	helper_setfuncs(L, fcgi_net_function_table);
	lua_setfield(L, -2, "net");
#endif

	/* create sub-lib "fcgi.timer" */
#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 502
	luaL_getsubtable(L, -1, "timer");
	luaL_setfuncs(L, fcgi_timer_function_table, 0);
	lua_pop(L, 1);
#else
	lua_newtable(L);
	helper_setfuncs(L, fcgi_timer_function_table);
	lua_setfield(L, -2, "timer");
#endif

	/* remove global from stack */
	lua_pop(L, 1);

	return(0);
}
