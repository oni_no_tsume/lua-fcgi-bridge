USE_LUA_LIB ?= lua

CC ?= gcc
STRIP ?= strip

SRCS := \
	lua-fcgi.c \
	functions.c

FLAGS := `pkg-config --cflags ${USE_LUA_LIB}`
LIBS := -lfcgi -lm `pkg-config --libs ${USE_LUA_LIB}`

all:
	${CC} -Os -ffunction-sections $(DEFS) $(FLAGS) -o lua-fcgi -I. $(SRCS) $(LIBS)
	${STRIP} lua-fcgi
	size lua-fcgi
